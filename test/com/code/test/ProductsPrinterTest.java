package com.code.test;

import com.code.model.Product;
import com.code.model.Size;
import org.junit.Test;

import java.util.*;

import static com.code.domain.ProductsPrinter.getAvailableProducts;
import static org.junit.Assert.*;

public class ProductsPrinterTest {
    @Test
    public void emptyTestCase() {
        Map<Integer, Integer> products = new HashMap<>();
        List<Size> sizes = new ArrayList<>();
        Map<Integer, Integer> stocks = new HashMap<>();

        assertEquals(getAvailableProducts(products, sizes, stocks), new TreeSet<>());
    }

    @Test
    public void allProductsHaveStock() {
        Map<Integer, Integer> products = new HashMap<>();
        List<Size> sizes = new ArrayList<>();
        Map<Integer, Integer> stocks = new HashMap<>();

        products.put(1, 1);
        products.put(2, 2);
        products.put(3, 3);
        products.put(4, 4);
        products.put(5, 5);

        sizes.add(new Size(1, 1, false, false));
        sizes.add(new Size(2, 2, false, false));
        sizes.add(new Size(3, 3, false, false));
        sizes.add(new Size(4, 4, false, false));
        sizes.add(new Size(5, 5, false, false));

        stocks.put(1, 1);
        stocks.put(2, 1);
        stocks.put(3, 1);
        stocks.put(4, 1);
        stocks.put(5, 1);

        TreeSet<Product> expected = new TreeSet<>();
        products.forEach((k,v) -> expected.add(new Product(k, v, false, false)));
        assertEquals(getAvailableProducts(products, sizes, stocks), expected);
    }

    @Test
    public void noneHasStock() {
        Map<Integer, Integer> products = new HashMap<>();
        List<Size> sizes = new ArrayList<>();
        Map<Integer, Integer> stocks = new HashMap<>();

        products.put(1, 1);
        products.put(2, 2);
        products.put(3, 3);
        products.put(4, 4);
        products.put(5, 5);

        sizes.add(new Size(1, 1, false, false));
        sizes.add(new Size(2, 2, false, false));
        sizes.add(new Size(3, 3, false, false));
        sizes.add(new Size(4, 4, false, false));
        sizes.add(new Size(5, 5, false, false));

        stocks.put(1, 0);
        stocks.put(2, 0);
        stocks.put(3, 0);
        stocks.put(4, 0);
        stocks.put(5, 0);

        TreeSet<Product> expected = new TreeSet<>();
        assertEquals(getAvailableProducts(products, sizes, stocks), expected);
    }

    @Test
    public void aBackSoonProductHaveStock() {
        Map<Integer, Integer> products = new HashMap<>();
        List<Size> sizes = new ArrayList<>();
        Map<Integer, Integer> stocks = new HashMap<>();

        products.put(1, 1);
        products.put(2, 2);
        products.put(3, 3);
        products.put(4, 4);
        products.put(5, 5);

        sizes.add(new Size(1, 1, false, false));
        sizes.add(new Size(2, 2, false, false));
        sizes.add(new Size(3, 3, true, false));
        sizes.add(new Size(4, 4, false, false));
        sizes.add(new Size(5, 5, true, false));

        stocks.put(1, 0);
        stocks.put(2, 0);
        stocks.put(3, 0);
        stocks.put(4, 0);
        stocks.put(5, 0);

        TreeSet<Product> expected = new TreeSet<>();
        expected.add(new Product(3, 3, false, true));
        expected.add(new Product(5, 5, false, true));

        TreeSet<Product> actual =  getAvailableProducts(products, sizes, stocks);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void aSpecialProductWithNoSpecialOFTheSameHaveStock() {
        Map<Integer, Integer> products = new HashMap<>();
        List<Size> sizes = new ArrayList<>();
        Map<Integer, Integer> stocks = new HashMap<>();

        products.put(1, 1);
        products.put(2, 2);
        products.put(3, 3);
        products.put(4, 4);
        products.put(5, 5);

        sizes.add(new Size(11, 1, false, true));
        sizes.add(new Size(12, 1, false, false));
        sizes.add(new Size(2, 2, false, false));
        sizes.add(new Size(3, 3, false, false));
        sizes.add(new Size(4, 4, false, false));
        sizes.add(new Size(5, 5, false, false));

        stocks.put(11, 1);
        stocks.put(12, 1);
        stocks.put(2, 0);
        stocks.put(3, 0);
        stocks.put(4, 0);
        stocks.put(5, 0);

        TreeSet<Product> expected = new TreeSet<>();
        expected.add(new Product(1, 1, true, true));

        TreeSet<Product> actual =  getAvailableProducts(products, sizes, stocks);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void aSpecialProductAloneDoesNotHaveStock() {
        Map<Integer, Integer> products = new HashMap<>();
        List<Size> sizes = new ArrayList<>();
        Map<Integer, Integer> stocks = new HashMap<>();

        products.put(1, 1);
        products.put(2, 2);
        products.put(3, 3);
        products.put(4, 4);
        products.put(5, 5);

        sizes.add(new Size(11, 1, false, true));
        sizes.add(new Size(12, 1, false, false));
        sizes.add(new Size(2, 2, false, false));
        sizes.add(new Size(3, 3, false, false));
        sizes.add(new Size(4, 4, false, false));
        sizes.add(new Size(5, 5, false, false));

        stocks.put(11, 1);
        stocks.put(12, 0);
        stocks.put(2, 0);
        stocks.put(3, 0);
        stocks.put(4, 0);
        stocks.put(5, 0);

        TreeSet<Product> expected = new TreeSet<>();

        TreeSet<Product> actual =  getAvailableProducts(products, sizes, stocks);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void theProductsHaveTheCorrectSequenceOrder() {
        Map<Integer, Integer> products = new HashMap<>();
        List<Size> sizes = new ArrayList<>();
        Map<Integer, Integer> stocks = new HashMap<>();

        int _1st = 10;
        int _2nd = 20;
        int _3rd = 30;
        int _4th = 40;
        int _5th = 50;

        products.put(5, _2nd);
        products.put(3, _3rd);
        products.put(4, _4th);
        products.put(1, _5th);
        products.put(2, _1st);

        sizes.add(new Size(1, 1, false, false));
        sizes.add(new Size(2, 2, false, false));
        sizes.add(new Size(3, 3, false, false));
        sizes.add(new Size(4, 4, false, false));
        sizes.add(new Size(5, 5, false, false));

        stocks.put(1, 1);
        stocks.put(2, 1);
        stocks.put(3, 1);
        stocks.put(4, 1);
        stocks.put(5, 1);

        Set<Product> expected = new LinkedHashSet<>();
        expected.add(new Product(2, _1st, false, true));
        expected.add(new Product(5, _2nd, false, true));
        expected.add(new Product(3, _3rd, false, true));
        expected.add(new Product(4, _4th, false, true));
        expected.add(new Product(1, _5th, false, true));

        TreeSet<Product> actual =  getAvailableProducts(products, sizes, stocks);
        Object[] expecteds = expected.toArray();
        Object[] actuals = actual.toArray();

        assertEquals(expecteds.length, actuals.length);
        for (int i = 0; i < expecteds.length; i++) {
            assertEquals(actuals[i], expecteds[i]);
        }
    }
}