package com.code;

import com.code.model.Product;
import com.code.model.Size;

import java.util.*;

import static com.code.ParseUtils.*;
import static com.code.domain.ProductsPrinter.getAvailableProducts;

// Complexity notes info
// p: Amount of products
// s: Amount of sizes
// Since the amount of stocks is bounded by the number of sizes, I will take the value of sizes as a reference. i.e: | stocks | <= | sizes |

public class App {

    public static void main(String[] args) {
        parseFiles();
    }

    private static void parseFiles() {
        Map<Integer, Integer> products = parseProducts();   // Complexity: O(p)
        List<Size> sizes = parseSizes();                    // Complexity: O(s)
        Map<Integer, Integer> stocks = parseStocks();       // Complexity: O(s)

        SortedSet<Product> sortedProducts = getAvailableProducts(products, sizes, stocks);    // Complexity: O(s) + O(p log p)

        printProductsThatHaveStock(sortedProducts); // Complexity: O(p): run through a set and printing an integer.
        // Total complexity: O(p) + 4 * O(s) + O(p log p) + O(p) = O(s) + O(p log p)
    }


    private static void printProductsThatHaveStock(SortedSet<Product> sortedProducts) {
        sortedProducts.forEach(p -> {   // Complexity: O(p): run through a set and printing an integer.
            System.out.println(p.getId());
        });
    }
}
