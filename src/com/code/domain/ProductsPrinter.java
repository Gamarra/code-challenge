package com.code.domain;

import com.code.model.Product;
import com.code.model.Size;
import com.code.model.SizeWithStock;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class ProductsPrinter {
    public static TreeSet<Product> getAvailableProducts(Map<Integer, Integer> products, List<Size> sizes, Map<Integer, Integer> stocks) {
        Map<Integer, Product> productsMap = new HashMap<>();    // Complexity: O(1)
        List<SizeWithStock> sizeWithStocks = getSizesWithStocks(sizes, stocks); // Complexity: O(s)

        sizeWithStocks.forEach(s -> {                           // Complexity: O(s), run through mapped sizes with stock
            if (s.getQuantity() > 0 || s.isBackSoon()) {
                int productId = s.getProductId();
                Product currentProduct = productsMap.get(productId);
                if (isNull(currentProduct)) {   // If currentProduct is null and has stock, then I add this product to the possible products set who has stock
                    Product product = new Product(
                            productId,
                            products.get(productId),
                            s.isSpecial(),
                            !s.isSpecial()      // If first product is special then I can't guarantee if product has stock
                    );
                    productsMap.put(productId, product);        // Complexity: O(1) , Insertion in a hash map
                } else {    // // Complexity: O(1) , block with boolean checks
                    if (currentProduct.isSpecial()) {   // If currentProduct is already in the possible products set, I check if it was special
                        if (!s.isSpecial()) { // here I check if product "s" is not special, just to comnfirm that special product has stock
                            currentProduct.setHasStock(true);  // now I can guarantee that product has stock, I set hasStock in currentProduct by it's reference
                        }
                    }
                }
            }
        });


        return productsMap.values().stream().filter(Product::hasStock).collect(Collectors.toCollection(TreeSet::new)); // Complexity: O(p log p) , insert p products in a sorted set

        // Total complexity of printAvailableProducts() analysis:  O(1) +  O(1) + O(s) + O(1) + O(s) + O(p log p) = O(s) + O(p log p)
    }

    private static List<SizeWithStock> getSizesWithStocks(List<Size> sizes, Map<Integer, Integer> stocks) {
        return sizes.stream()     // Complexity: O(s)
                .map(s ->
                        new SizeWithStock(                      // Complexity: O(1) (initialize SizeWithStock object)
                                s.getId(),
                                s.getProductId(),
                                s.isBackSoon(),
                                s.isSpecial(),
                                isNull(stocks.get(s.getId()))? 0 : stocks.get(s.getId()))
                )
                .collect(Collectors.toList());
    }

}
