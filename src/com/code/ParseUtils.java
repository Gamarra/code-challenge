package com.code;

import com.code.model.Size;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Boolean.parseBoolean;
import static java.lang.Integer.parseInt;

public class ParseUtils {
    public static Map<Integer, Integer> parseProducts() {
        System.out.println("Parsing products: ");
        List<List<String>> productsAsList = parseFile("code-challenge/src/resources/product.csv");
        Map<Integer, Integer> products = new HashMap<>();
        productsAsList.forEach(p -> products.put(parseInt(p.get(0)), parseInt(p.get(1))));
        return products;
    }

    public static List<Size> parseSizes() {
        System.out.println("Parsing sizes: ");
        List<List<String>> sizesAsList = parseFile("code-challenge/src/resources/size.csv");
        return sizesAsList.stream()
                .map(p -> new Size(parseInt(p.get(0)), parseInt(p.get(1)), parseBoolean(p.get(2)), parseBoolean(p.get(3))))
                .collect(Collectors.toList());
    }

    public static Map<Integer, Integer> parseStocks() {
        System.out.println("Parsing stock: ");
        List<List<String>> stocksAsList = parseFile("code-challenge/src/resources/stock.csv");
        Map<Integer, Integer> stocks = new HashMap<>();
        stocksAsList.forEach(p -> stocks.put(parseInt(p.get(0)), parseInt(p.get(1))));
        return stocks;
    }

    public static List<List<String>> parseFile(String filePath) {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(", ");
                records.add(Arrays.asList(values));
            }
            System.out.println("- done.");
        } catch (IOException exception) {
            System.out.println("Error opening file: " + exception.getMessage());
        }

        return records;
    }
}
