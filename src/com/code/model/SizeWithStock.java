package com.code.model;

public class SizeWithStock {
    private int id;
    private int productId;
    private boolean backSoon;
    private boolean special;
    private int quantity;

    public SizeWithStock(int id, int productId, boolean backSoon, boolean special, int quantity) {
        this.id = id;
        this.productId = productId;
        this.backSoon = backSoon;
        this.special = special;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public int getProductId() {
        return productId;
    }

    public boolean isBackSoon() {
        return backSoon;
    }

    public boolean isSpecial() {
        return special;
    }

    public int getQuantity() {
        return quantity;
    }
}
