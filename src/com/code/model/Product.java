package com.code.model;

public class Product implements Comparable<Product> {
    private int id;
    private int sequence;
    private boolean special;
    private boolean hasStock;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != product.id) return false;
        if (sequence != product.sequence) return false;
        if (special != product.special) return false;
        return hasStock == product.hasStock;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + sequence;
        result = 31 * result + (special ? 1 : 0);
        result = 31 * result + (hasStock ? 1 : 0);
        return result;
    }

    public Product(int id, int sequence, boolean special, boolean hasStock) {
        this.id = id;
        this.sequence = sequence;
        this.special = special;
        this.hasStock = hasStock;
    }

    public int getId() {
        return id;
    }

    public int getSequence() {
        return sequence;
    }

    public boolean isSpecial() {
        return special;
    }

    public boolean hasStock() {
        return hasStock;
    }

    public void setHasStock(boolean hasStock) {
        this.hasStock = hasStock;
    }

    @Override
    public int compareTo(Product p) {
        return Integer.compare(this.sequence, p.sequence);
    }
}
