# Code Challenge

Visibility Algorithm

## Time complexity analysis and structures

For parsing each file I used the following data structures:

`Map<Integer, Integer> products`


`List<Size> sizes`


`Map<Integer, Integer> stocks`

I used maps for products and stocks since after parsing I need to obtain the elements inside a loop, and using a Map is more efficient since find here runs in O(1).
In com/code/Main.java:40, I start initializing a Map of products but with its availability in order to save if a product really has stock.

Later, I filter products with stock, and I insert all these products in a SortedSet using a specific comparator defined in com.code.model.Product.compareTo. I found it the best way to optimize this by just sort the filtered amount of products without repeated elements.

Finally, I just print the filtered and sorted products.

Complexity:  O(s) + O(p log p) . O(s) is the time cost to make all the parsing, and the mapping to other data structures. O (p log p) is the cost of mapping products, transforming it in different linear data structures, and sorting these products.

I consider that there is no better way to make it better in terms of time complexity since I'll need to parse all the data and I'll need to make a sort of the products. that is, there is no other algorithm that runs in less time in terms of algorithmic complexity than O(s) and O(p log p) .
